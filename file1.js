// back tics,single quotes,double qutoes can be used
console.log("hello world");
// declare a variable
var firstName = "divya";
// we can later use variable
console.log(firstName);
// changing variable value
firstName = "sami";
// after changing the value print again
console.log(firstName);
// rules for naming variables
var value1 = 10;
// console.log(value1 /2 );
console.log(value1 ** 2 );
var $firstname = "sami";
console.log($firstname);
// declare const
 const pi = 3.14; 
console.log(pi);