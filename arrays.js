 let fruit = ["cherry", "mango", "orange"];
 let numbers = [1,2,3,4];
 let mixed = [1,2,2.3, "string", null, undefined];
 console.log(mixed);
 console.log(numbers);
 console.log(fruit[2]);

let fruits = ["apple", "mango", "orange"];
let obj = {}; // object literal
 console.log(fruits);
 fruits[1] = "banana";
 console.log(fruits);
console.log(typeof fruits);
console.log(typeof obj);
console.log(Array.isArray(fruits));
console.log(Array.isArray(obj));
let fru = ["apple", "mango", "grapes"];
console.log(fru);
// push 
fruits.push("banana");
console.log(fruits);
// pop 
let poppedFruit = fruits.pop();
console.log(fruits);
console.log("popped fruits is", poppedFruit);
// unshift 
fruits.unshift("banana");
fruits.unshift("myfruit");
console.log(fruits);
// shift 
let removedFruit = fruits.shift();
console.log(fruits);
console.log("removed fruits is ", removedFruit);
let num1 = 6;
// primitive data types---
let num2 = num1;
console.log("value is num1 is", num1);
console.log("value is num2 is", num2);
num1++;
console.log("after incrementing num1")
console.log("value is num1 is", num1);
console.log("value is num2 is", num2);
// reference types  
let array1 = ["item1", "item2"];
let array2 = array1;
console.log("array1", array1);
console.log("array2", array2);
array1.push("item3");
console.log("after pushing element to array 1");
console.log("array1", array1);
console.log("array2", array2);
// how to concatenate two arrays
 let array3 = ["item1", "item2"];
 let array4 = ["item1", "item2"];
let array5 = array3.slice(0).concat(["item3", "item4"]);
 let array6 = [].concat(array3,["item3", "item4"]); 
// spread operator
let oneMoreArray = ["item3", "item4"]
let array7 = [...array1, ...oneMoreArray];

array1.push("item3");

console.log(array3===array4);
console.log(array3)
console.log(array4)
// for loop in array 

let Sfruits = ["apple", "mango", "grapes", "banana"];

for(let i=0; i<=9;i++){
    console.log(i);
}
console.log(Sfruits.length);
console.log(Sfruits[Sfruits.length-2]);
let Wfruits2 = [];
for(let i=0; i < Sfruits.length; i++){
    Wfruits2.push(Sfruits[i].toUpperCase());
}
console.log(Wfruits2);

// const for creating array 
const Dfruits = ["apple", "mango"]; 
 Dfruits.push("banana");
 console.log(Dfruits);

// while loop in array 
const ffruits = ["apple", "mango", "grapes"];
const ffruits2 = [];
let i = 0;
while(i<ffruits.length){
    ffruits2.push(ffruits[i].toUpperCase());
    i++;
}
console.log(ffruits2);
// for in loop in array
const deserts = ["jamun", "jalebi", "rasmali", "desert4", "desert5"];
const deserts2 = [];

for(let index in deserts){
    deserts2.push(deserts[index].toUpperCase());
}
console.log(deserts2);
// array destructuring 
const myArray = ["value1", "value2", "value3","value4"];
let myvar = myArray[0];
let myvars = myArray[1];
console.log("value of myvar", myvar);
console.log("value of myvars", myvars);
let [myvari, myvar2, ...myNewArray] = myArray;
console.log("value of myvar", myvar);
console.log("value of myvars", myvar2);
console.log(myNewArray);



