// string indexing it starts from and it increases and spaces also included
let first_Name = "sameeksha";
console.log(first_Name[3]);
console.log(first_Name.length);
// last index : length-1
let first_name = "sAMeeksha";
 console.log(first_name.length);
 first_name.trim();
 console.log(first_name.length);
//  provides new string
first_name = first_name.toUpperCase();
 console.log(first_name);
 first_name = first_name.toLowerCase();
 console.log(first_name);
//  start index and end index for slicing
let newString = first_name.slice(0,2);
console.log(newString);
// typeof operator usage
let age = 22;
let $firstname = "srilaxmi"; 
console.log(typeof age);
console.log(typeof $firstname);
console.log(typeof 7);
// convert number to string
age = age +"";
console.log(typeof(age )); "22"
// covert string to number
let mystr = +"34";
console.log(typeof mystr);
// string concatenation
let string1 = "vaishali";
let string2 = "nandu";
let fullName = string1 +" "+  string2;
console.log(fullName);
let string3 = "17";
let string4 = "10";
let name = string3 +" "+  string4;
console.log(name);
// template string
let age1 = 22;
let _name = "vani"
let aboutMe = "my name is " + _name + " and my age1 is " + age; 
console.log(aboutMe);
// we can overcome about method by using template string
let aboutme = `my name is ${_name} and my age is ${age1}`
console.log(aboutMe);
// undefined -- these we cannot perform on const we get an error
let name1;
console.log(typeof name1);
// null
let myVariable = null;
console.log(myVariable);
myVariable = "monk";
console.log(myVariable, typeof myVariable);
// BigInt
let myNumber = BigInt(12);
// console.log(Number.MAX_SAFE_INTEGER);
// to perform any operation both should be BigInt 
let sameMyNumber = 123n;
console.log(myNumber+ sameMyNumber);











