// filter method 

const num = [1,3,2,6,4,8];
const evenNumbers = num.filter((number)=>{
    return number % 2 === 0;
});
console.log(evenNumbers);
//   sort method
const userNames = ['harshit', 'abcd', 'mohit', 'nitish', 'aabc', 'ABC', 'Harshit'];
userNames.sort();
console.log(userNames);

const numbers = [5,9,1200, 410, 3000];
numbers.sort((a,b)=>{
    return b-a;
});
numbers.sort((a,b)=>a-b);
console.log(numbers);

// 1200,410 
// a-b ---> 790
// a-b ---> postive (greater than 0) ---> b, a
// 410 , 1200

// a-b ---> negative ----> a,b
// 5, 9 ---> -4 
// price lowToHighest HighToLow 
const products = [
    {productId: 1, produceName: "p1",price: 300 },
    {productId: 2, produceName: "p2",price: 3000 },
    {productId: 3, produceName: "p3",price: 200 },
    {productId: 4, produceName: "p4",price: 8000 },
    {productId: 5, produceName: "p5",price: 500 },
]

// lowToHigh
const lowToHigh = products.slice(0).sort((a,b)=>{
    return a.price-b.price
});

const highToLow = products.slice(0).sort((a,b)=>{
    return b.price-a.price;
});
const users = [
    {firstName: "vani", age: 23},
    {firstName: "pra", age: 21},
    {firstName: "lisa", age: 22},
    {firstName: "jay", age: 20},
]
users.sort((a,b)=>{
    if(a.firstName > b.firstName){
        return 1;
    }else{
        return -1;
    }
});
console.log(users);
// find method 
const myArray = ["Hello", "mouse", "dog", "lion"];
function isLength3(string){
    return string.length === 3;
}
const ans6 = myArray.find((string)=>string.length===3);
console.log(ans6);

const user = [
    {userId : 1, userName: "pra"},
    {userId : 2, userName: "vani"},
    {userId : 3, userName: "lucky"},
    {userId : 4, userName: "madhu"},
    {userId : 5, userName: "lusy"},
];
const myUser = user.find((user)=>user.userId===3);
console.log(myUser);
// every method
const number = [2,4,6,9,10];
const ans7 = numbers.every((number)=>number%2===0);
console.log(ans7);
const userCart = [
    {productId: 1, productName: "mobile", price: 12000},
    {productId: 2, productName: "laptop", price: 22000},
    {productId: 3, productName: "tv", price: 35000},
]
const ans9 = userCart.every((cartItem)=>cartItem.price < 30000);
console.log(ans9);
// some method 

const number1 = [3,5,11,9];const ans = numbers.some((number)=>number%2===0);
console.log(ans);

const userCart1 = [
    {productId: 1, productName: "mobile", price: 12000},
    {productId: 2, productName: "laptop", price: 22000},
    {productId: 3, productName: "tv", price: 35000},
    {productId: 3, productName: "macbook", price: 25000},
]
const ans8 = userCart.some((cartItem)=>cartItem.price > 100000);
console.log(ans8);
// fill method 
const myArray1 = new Array(10).fill(0);
console.log(myArray);
const myArray2 = [1,2,3,4,5,6,7,8];
myArray.fill(0,2,5);
console.log(myArray);
// splice method 
// start , delete , insert 
const myArray5 = ['item1', 'item2', 'item3'];
const deletedItem = myArray.splice(1, 2);
console.log("delted item", deletedItem);
myArray.splice(1, 0,'inserted item');
// insert and delete 
const deletedItem1 = myArray.splice(1, 2, "inserted item1", "inserted item2")
console.log("delted item", deletedItem);
console.log(myArray);



