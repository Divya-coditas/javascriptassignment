function hello(){
    console.log("hello world");
}
console.log(hello.name); 
hello.myOwnProperty = "very unique value";
console.log(hello.myOwnProperty);
console.log(hello.prototype); // {}

hello.prototype.abc = "abc";
hello.prototype.xyz = "xyz";
hello.prototype.sing = function(){
    return "lalalla";
};
console.log(hello.prototype.sing());
