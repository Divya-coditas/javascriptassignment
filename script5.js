const userMethods = {
    about : function(){
        return `${this.firstName} is ${this.age} years old.`;
    },
    is18 : function(){
        return this.age >= 18;
    },
    sing: function(){
        return 'la ala laaaa ';
    }
}
function createUser(firstName, lastName, email, age, address){
    const user = Object.create(createUser.prototype);// {}
    user.firstName = firstName;
    user.lastName = lastName;
    user.email = email;
    user.age = age;
    user.address = address;
    return user;
}
createUser.prototype.about = function(){
    return `${this.firstName} is ${this.age} years old.`;
};
createUser.prototype.is18 = function (){
    return this.age >= 18; 
}
createUser.prototype.sing = function (){
    return "na na nannna  ";
}
const user1 = createUser('vinay', 'goli', 'vinay@gmail.com', 18, "my address");
const user2 = createUser('vishwa', 'kathi', 'vishwa@gmail.com', 19, "my address");
const user3 = createUser('lily', 'viman', 'lily@gmail.com', 17, "my address");
console.log(user1);
console.log(user1.is18());