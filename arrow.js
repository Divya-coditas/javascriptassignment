const user1 = {
    firstName : "teju",
    age: 8,
    about: () => {
        console.log(this.firstName, this.age);
    }   
}
user1.about(user1);
const user2 = {
        firstName : "vani",
        age: 8,
        about: function(){
            console.log(this.firstName, this.age);
        }   
    }
    
    const user3 = {
        firstName : "latha",
        age: 8,
        about(){
            console.log(this.firstName, this.age);
        }   
    }
    user1.about();
    
