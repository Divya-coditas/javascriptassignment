function CreateUser(firstName, lastName, email, age, address){
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.age = age;
    this.address = address;
}
CreateUser.prototype.about = function(){
    return `${this.firstName} is ${this.age} years old.`;
};
CreateUser.prototype.is18 = function (){
    return this.age >= 18; 
}
CreateUser.prototype.sing = function (){
    return "oh na na na na ";
}
const user1 = new CreateUser('lily', 'vashsith', 'lily@gmail.com', 18, "my address");
const user2 = new CreateUser('viya', 'vashsith', 'viya@gmail.com', 19, "my address");
const user3 = new CreateUser('lakshaya', 'vashsitha', 'lak@gmail.com', 17, "my address");
for(let key in user1){
    console.log(key);
    if(user1.hasOwnProperty(key)){
        console.log(key);
    }
}

