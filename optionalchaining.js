// optional chaining 
const user  = {
    firstName: "divya",
}
console.log(user?.firstName);
console.log(user?.address?.houseNumber);
// methods
// function inside object

function personInfo(){
    console.log(`person name is ${this.firstName} and age is ${this.age}`);
}
const person1 = {
    firstName : "harsh",
    age: 8,
    about: personInfo
}
const person2 = {
    firstName : "pranu",
    age: 18,
    about: personInfo
}
const person3 = {
    firstName : "latha",
    age: 17,
    about: personInfo
}
person1.about();
person2.about();
person3.about();


