const person1 = {name:"divya",age:22};
const person = {
    name: "divya",
    age: 22,
    hobbies: ["claymoulding", "sleeping", "reading books"]
}
console.log(person);

//  access data from objects 
 console.log(person["name"]);
 console.log(person["age"]);
 console.log(person.hobbies);
//  ----------------//
const key1 = "objkey1";
const key2 = "objkey2";

const value1 = "myvalue1";
const value2 = "myvalue2";

 const obj = {
     objkey1 : "myvalue1",
    objkey2 : "myvalue2",
 }

 const obj3 = {
     [key1] : value1,
    [key2] : value2
 }

const obj4 = {};

obj[key1] = value1;
obj[key2] = value2;
console.log(obj);
// spread operator
 const array1 = [1, 2, 3];
 const array2 = [5, 6, 7];
const newArray = [...array1, ...array2, 89, 69];
 const newArray3 = [..."123456789"];
 console.log(newArray3);

// spread operator in objects
const obj1 = {
    key1: "value1",
    key2: "value2",
  };
  const obj2 = {
    key1: "valueUnique",
    key3: "value3",
    key4: "value4",
  };
   const newObject5 = { ...obj2, ...obj1, key69: "value69" };
   const newObject6 = { ...["value1", "value2"] };
   const newObject = { ..."jklmnopqrstuvwxyz" };
   console.log(newObject);
// object destructuring
const band = {
    bandName: "DJKV",
    famousSong: "dear zindagi",
    year: 2000,
    anotherFamousSong: "anu anu",
  };
  let { bandName, famousSong, ...restProps } = band;
  console.log(bandName);
  console.log(restProps);
  // objects inside array

const users = [
    {userId: 1,firstName: 'divya', gender: 'female'},
    {userId: 2,firstName: 'nandu', gender: 'female'},
    {userId: 3,firstName: 'vansh', gender: 'male'},
]
for(let user of users){
    console.log(user.firstName);
}
//  nested destructuring 
const user = [
    {userId: 1,firstName: 'divya', gender: 'female'},
    {userId: 2,firstName: 'jeveetha', gender: 'female'},
    {userId: 3,firstName: 'raj', gender: 'male'},
]

const [{firstName: user1firstName, userId}, , {gender: user3gender}] = users;
console.log(user1firstName);
console.log(userId);
console.log(user3gender);


