const userMethods = {
    about : function(){
        return `${this.firstName} is ${this.age} years old.`;
    },
    is18 : function(){
        return this.age >= 18;
    },
    sing: function(){
        return 'ha ha haaaaaaa ';
    }
}
function createUser(firstName, lastName, email, age, address){
    const user = Object.create(userMethods);// {}
    user.firstName = firstName;
    user.lastName = lastName;
    user.email = email;
    user.age = age;
    user.address = address;
    return user;
}

const user1 = createUser('priya', 'vashsith', 'priya@gmail.com', 9, "my address");
const user2 = createUser('lulu', 'lulu', 'lulu@gmail.com', 19, "my address");
const user3 = createUser('bob', 'vashsitha', 'bob@gmail.com', 17, "my address");
console.log(user1);
console.log(user1.about());

