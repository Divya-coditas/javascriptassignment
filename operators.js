//  booleans & comparison operator
let num1 = 5;
let num2 = 7;
console.log(num1 > num2);
// == vs ===
console.log(num1 === num2);
console.log(num1 < num2);
console.log(num1 != num2);
// if else condition
let age = 17;
if (age > 18) {
    console.log("user can play ddlc");
} else {
    console.log("user can play mario");
}
let num = 14;
if (num % 2 === 0) {
    console.log("even");
} else {
    console.log("odd");
}
// falsy values
// let name_ ="";
// let name_ = null;
// truthy
let name_ = "pranavi";
if (name_) {
    console.log(name_);
} else {
    console.log("name_ is  kinda empty");
}
// ternary operator
let age2 = 15;
let drink;
if (age >= 5) {
    drink = "cofee";
} else {
    drink = "milk";
}
console.log(drink);
// by using ternary operator / conditional operator reduce these  above code
let age3 = 8;
let drinks = age >= 5 ? "cofee" : "milk";
console.log(drinks);
// and or operator
let pName = "sherly";
let age4 = 22;
if (pName[0] === "s") {
    console.log("your name starts with s")
}
if (age4 > 18) {
    console.log("you are above 18");
}
if (pName[0] === "s" && age4 > 18) {
    console.log("pName starts with s and above 18")
} else {
    console.log("inside else");
}
// nested if else
// winning number 19
// 19 your guess is right
// 17 too low
// 20 too high
let winningNumber = 19;
let userGuess = +prompt("Guess a number");
// if else inside we are using if else which is nested if else
if (userGuess === winningNumber) {
    console.log("your guess is right!!");
} else {
    if (userGuess < winningNumber) {
        console.log("too low !!!");
    } else {
        console.log("too high !!!");
    }
}
// using if , else if
let day = 0;
if (day === 0) {
    console.log("sunday");
} else if (day === 1) {
    console.log("Monday");
} else if (day === 2) {
    console.log("Tuesday");
} else if (day === 3) {
    console.log("Wednesday");
} else if (day === 4) {
    console.log("Thursday");
} else if (day === 5) {
    console.log("Friday");
} else if (day === 6) {
    console.log("Saturday");
} else {
    console.log("Invalid Day");
}
// while loop
let i = 0;

while (i <= 6) {
    console.log(i);
    i++;
}
console.log(`current value of i is ${i}`);
console.log("welcome");
// while loop
let num5 = 100;
let total1 = 0; //1 + 2 +3
let k = 0;
while (k <= 100) {
    total1 = total1 + k;
    k++;
}
console.log(total1);
let total2 = (num * (num + 1)) / 2;
console.log(total2);
// for loop
for(let i = 0;i<=9;i++){
    console.log(i);
}
// break keyword
for(let i = 1; i<=10; i++){
    if(i===4){
        break;
    }
    console.log(i);
} 
// continue
for(let i = 1; i<=10; i++){
     if(i===4){
         continue;
     }
     console.log(i);
 }
console.log("hello world");
// do while loop
while(i<=9){
    console.log(i);
    i++;
}

let j = 10;
do{
    console.log(i);
    j++;
}while(i<=9);

console.log("value of i is ", i);


