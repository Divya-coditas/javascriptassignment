const firstName = "mounika";
for(let char of firstName){
    console.log(char);
}
const items = ['item1', 'item2', 'item3'];
for(let item of items){
    console.log(item);
}
// array like object 
const firstName1 = "teju";
console.log(firstName.length);
console.log(firstName[2]);
const items1 = ['item1', 'item2', 'item3'];
const numbers = new Set();
numbers.add(1);
numbers.add(2);
numbers.add(3);
numbers.add(4);
numbers.add(5);
numbers.add(6);
numbers.add(items);
if(numbers.has(1)){
    console.log("1 is present")
}else{
    console.log("1 is not present")
}
for(let number of numbers){
    console.log(number);
}
const myArray = [1,2,4,4,5,6,5,6];
const uniqueElements = new Set(myArray);
let length = 0;
for(let element of uniqueElements){
    length++;
}

console.log(length);
// maps
const person = {
        firstName : "vani",
        age: 7,
        1:"one"
    }
     console.log(person.firstName);
     console.log(person["firstName"]);
     console.log(person[1]);
     for(let key in person){
        console.log(typeof key);
    }
    const person1 = new Map();
    person1.set('firstName','vani');
    person1.set('age', 7);
    person1.set(1,'one');
    person1.set([1,2,3],'onetwothree');
    person1.set({1: 'one'},'onetwothree');
    console.log(person1);
    console.log(person1.get(1));
    for(let key of person1.keys()){
        console.log(key, typeof key);
    }
    for(let [key, value] of person1){
         console.log(Array.isArray(key));
        console.log(key, value)
    }
    const person2 = {
        id: 1,
        firstName: "vani"
    }
    const person3 = {
        id: 2,
        firstName: "vani"
    }
    const extraInfo = new Map();
    extraInfo.set(person1, {age: 8, gender: "male"});
    extraInfo.set(person2, {age: 9, gender: "female"});
    console.log(person1);
    console.log(person2.id);
    console.log(extraInfo.get(person1).gender);
    console.log(extraInfo.get(person2).gender);
    

    
