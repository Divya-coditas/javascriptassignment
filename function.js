function singHappyBirthday(){
    console.log("happy birthday to you ");
}

function sumThreeNumbers(number1, number2, number3){
    return number1 + number2 + number3;
} 

function isEven(number){
    return number % 2 === 0;
}

console.log(isEven(4)); 

function firstChar(anyString){
    return anyString[0];
}

console.log(firstChar("fgh"));
 function findTarget(array, target){
    for(let i = 0; i<array.length; i++){
        if(array[i]===target){
            return i;
        }
    }
    return -1;
}
const myArray = [1,3,8,90]
const ans = findTarget(myArray, 4);
console.log(ans);
const singHappyBirthdaysong = () => {
    console.log("happy birthday to you ......");
}

singHappyBirthday();

const sumThreeNumbers1 = (number1, number2, number3) => {
    return number1 + number2 + number3;
}

const ans1 = sumThreeNumbers(2,3,4);
console.log(ans1);

 const isEvennum = function(number){
     return number % 2 === 0;
 }

const isEvennum1 = number => number % 2 === 0;


console.log(isEven(4));

const firstChar1 = anyString => anyString[0];

console.log(firstChar("divya"));


const findTarget1 = (array, target) => {
    for(let i = 0; i<array.length; i++){
        if(array[i]===target){
            return i;
        }
    }
    return -1;
}
//  hoisting 
helloworld();
function helloworld(){
    console.log("hello world welcome to new work");
}
console.log(helloworld);
const hello = "hello world welcome to new work";
console.log(helloworld);
// functions inside function 
function app(){
    const myFunc = () =>{
        console.log("function inside function")
    }
    
    const addTwo = (num1, num2) =>{
        return num1 + num2;
    }

    const mul = (num1, num2) => num1* num2;

    console.log("inside app");
    myFunc();
    console.log(addTwo(2,3));
    console.log(mul(2,3));
}
app();
// lexical scope 
const myVar = "value1";
function myApp(){
    function myFunc(){
        const myFunc2 = () =>{
 console.log("inside myFunc", myVar);
        }
        myFunc2();
    }
console.log(myVar);
    myFunc();
}
myApp();
// block vs scope
if(true){
         var firstName = "divya";
         console.log(firstName);
        }
    console.log(firstName);
    function myApp(){
        if(true){
            var firstName = "divya";
            console.log(firstName);
        }
    if(true){
            console.log(firstName);
        }
        console.log(firstName);
}
    myApp();
    // default parameters 
function addTwo(a,b){
        if(typeof b ==="undefined"){
            b = 0;
        }
        return a+b;
    }
    function addTwo(a,b=0){
        return a+b;
    }
    const ans3 = addTwo(4, 8);
    console.log(ans3);
    
    // rest parameters 
function myFunc(a,b,...c){
    console.log(`a is ${a}`);
    console.log(`b is ${b}`);
    console.log(`c is`, c);
}
myFunc(3,4,5,6,7,8,9);

function addAll(...numbers){
    let total = 0;
    for(let number of numbers){
        total = total + number;
    }
    return total;
}

const ans4 = addAll(4,5,4,2,10);
console.log(ans4);
// param destructuring 
const person = {
    firstName: "divya",
    gender: "female",
    age: 500
}
function printDetails(obj){
    console.log(obj.firstName);
    console.log(obj.gender);
}
function printDetails({firstName, gender, age}){
    console.log(firstName);
    console.log(gender);
    console.log(age);
}
printDetails(person);
  // function returning function 
function myFunc(){
    function hello(){
        return "hello world"
    }
    return hello;
}
// const get = myFunc();
// console.log(get());
// important array methods 

const numbers = [4,2,5,8];
function myFunc(number, index){
     console.log(`index is ${index} number is ${number}`);
 }
numbers.forEach(function(number,index){
     console.log(`index is ${index} number is ${number}`);
 });

 numbers.forEach(function(number, index){
    console.log(number*3, index);
 })
const users = [
     {firstName: "vani", age: 23},
    {firstName: "pra", age: 21},
    {firstName: "sus", age: 22},
    {firstName: "lisa", age: 20},
]
users.forEach(function(user){
     console.log(user.firstName);
 });
users.forEach((user, index)=>{
     console.log(user.firstName, index);
 })
 for(let user of users){
     console.log(user.firstName);
 }
 // map method 

 const numbe = [3,4,6,1,8];

const square = function(number){
    return number*number;
}

const squareNumber = numbers.map((number, index)=>{
    return index;
});
console.log(squareNumber);

const user1 = [
    {firstName: "mani", age: 23},
    {firstName: "liya", age: 21},
    {firstName: "poo", age: 22},
    {firstName: "guss", age: 20},
]
const userNames = user1.map((user1)=>{
    return user1.firstName;
});
console.log(userNames);


  
    



